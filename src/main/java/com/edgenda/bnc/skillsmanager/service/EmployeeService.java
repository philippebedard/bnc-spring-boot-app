package com.edgenda.bnc.skillsmanager.service;

import com.edgenda.bnc.skillsmanager.model.Employee;
import com.edgenda.bnc.skillsmanager.model.Skill;
import com.edgenda.bnc.skillsmanager.repository.EmployeeRepository;
import com.edgenda.bnc.skillsmanager.repository.SkillRepository;
import com.edgenda.bnc.skillsmanager.service.exception.UnknownEmployeeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
@RestController
@RequestMapping(path="/employees")
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final SkillRepository skillRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, SkillRepository skillRepository) {
        this.employeeRepository = employeeRepository;
        this.skillRepository = skillRepository;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public Employee getEmployee(@PathVariable Long id) {
        Assert.notNull(id, "Employee ID cannot be null");
        return employeeRepository.findById(id)
                .orElseThrow(() -> new UnknownEmployeeException(id));
    }

    @RequestMapping
    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody Employee employee) {
        Assert.notNull(employee, "Employee cannot be null");
        final Employee newEmployee = new Employee(
                employee.getFirstName(),
                employee.getLastName(),
                employee.getEmail(),
                Collections.emptyList()
        );
        return employeeRepository.save(newEmployee);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateEmployee(@RequestBody Employee employee) {
        Assert.notNull(employee, "Employee cannot be null");
        this.getEmployee(employee.getId());
        employeeRepository.save(employee);
    }

    @RequestMapping(path = "/{employeeId}/skills",method = RequestMethod.GET)
    public List<Skill> getEmployeeSkills(@PathVariable Long employeeId) {
        return skillRepository.findByEmployeeId(employeeId);
    }

    @RequestMapping(path = "/{id}/skills",method = RequestMethod.DELETE)
    public void deleteEmployee(@PathVariable Long id) {
        Assert.notNull(id, "ID cannot be null");
        employeeRepository.delete(id);
    }
}
